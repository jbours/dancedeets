<?php

namespace JBours\DanceDeets\Entities;

use JBours\DanceDeets\Traits\IdAwareEntityTrait;
use JBours\DanceDeets\Traits\NameAwareEntityTrait;

class Admin
{
    use IdAwareEntityTrait;
    use NameAwareEntityTrait;
}
