<?php

namespace JBours\Tests\DanceDeets\Entities;

use ArgumentCountError;
use JBours\DanceDeets\Entities\Address;
use JBours\DanceDeets\Entities\GeometryPoint;
use JBours\DanceDeets\Entities\Venue;
use JBours\Tests\DanceDeets\TestCase;
use TypeError;

class VenueTest extends TestCase
{
    protected $venue;

    public function setUp()
    {
        parent::setUp();

        $this->venue = new Venue();
    }

    /**
     * @test
     */
    public function itCanSetAStringValueForId()
    {
        $this->venue->setId('12345678');

        $this->assertEquals('12345678', $this->venue->getId());
    }

    /**
     * @test
     */
    public function itCannotSetANullValueForId()
    {
        $this->venue->setId(null);

        $this->assertNull($this->venue->getId());
    }

    /**
     * @test
     */
    public function itCannotSetNoValueForId()
    {
        $this->expectException(ArgumentCountError::class);

        $this->venue->setId();
    }

    /**
     * @test
     */
    public function itCanSetAnAddressObjectAsValueForAddress()
    {
        $address = new Address();
        $address->setStreet('Some street 1');
        $address->setCity('Some City');
        $address->setZip('1000AA');
        $address->setCountryCode('nl');
        $address->setCountry('Netherlands');

        $this->venue->setAddress($address);

        $this->assertEquals($address, $this->venue->getAddress());
        $this->assertInstanceOf(Address::class, $this->venue->getAddress());
    }

    /**
     * @test
     */
    public function itCanSetAEmptyAddressObjectAsValueForAddress()
    {
        $address = new Address();

        $this->venue->setAddress($address);

        $this->assertEquals($address, $this->venue->getAddress());
        $this->assertInstanceOf(Address::class, $this->venue->getAddress());
    }

    /**
     * @test
     */
    public function itCannotSetNullAsValueForAddress()
    {
        $this->expectException(TypeError::class);

        $this->venue->setAddress(null);
    }

    /**
     * @test
     */
    public function itCannotSetNoValueForAddress()
    {
        $this->expectException(ArgumentCountError::class);

        $this->venue->setAddress();
    }

    /**
     * @test
     */
    public function itCanSetAnGeometryPointObjectAsValueForGeocode()
    {
        $geometryPoint = new GeometryPoint();
        $geometryPoint->setLatitude(53.2181718);
        $geometryPoint->setLongitude(6.5686807);

        $this->venue->setGeocode($geometryPoint);

        $this->assertEquals($geometryPoint, $this->venue->getGeocode());
        $this->assertInstanceOf(GeometryPoint::class, $this->venue->getGeocode());
    }

    /**
     * @test
     */
    public function itCanSetAEmptyGeometryPointObjectAsValueForGeocode()
    {
        $geometryPoint = new GeometryPoint();

        $this->venue->setGeocode($geometryPoint);

        $this->assertEquals($geometryPoint, $this->venue->getGeocode());
        $this->assertInstanceOf(GeometryPoint::class, $this->venue->getGeocode());
    }

    /**
     * @test
     */
    public function itCannotSetNullAsValueForGeocode()
    {
        $this->expectException(TypeError::class);

        $this->venue->setGeocode(null);
    }

    /**
     * @test
     */
    public function itCannotSetNoValueForGeocode()
    {
        $this->expectException(ArgumentCountError::class);

        $this->venue->setGeocode();
    }
}
