<?php

namespace JBours\Tests\DanceDeets\Traits;

use ArgumentCountError;
use JBours\DanceDeets\Traits\IdAwareEntityTrait;
use JBours\Tests\DanceDeets\TestCase;
use TypeError;

class IdAwareEntityTraitTest extends TestCase
{
    protected $stub;

    public function setUp()
    {
        parent::setUp();

        $this->stub = $this->getMockForTrait(IdAwareEntityTrait::class);
    }

    /**
     * @test
     */
    public function itCanSetAStringValueForId()
    {
        $this->stub->setId('12345678');

        $this->assertEquals('12345678', $this->stub->getId());
    }

    /**
     * @test
     */
    public function itCannotSetANullValueForId()
    {
        $this->expectException(TypeError::class);

        $this->stub->setId(null);
    }

    /**
     * @test
     */
    public function itCannotSetNoValueForId()
    {
        $this->expectException(ArgumentCountError::class);

        $this->stub->setId();
    }
}
