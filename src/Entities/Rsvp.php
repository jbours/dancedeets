<?php
declare(strict_types=1);

namespace JBours\DanceDeets\Entities;

class Rsvp
{
    /**
     * @var int
     */
    protected $attending_count;

    /**
     * @var int
     */
    protected $maybe_count;

    /**
     * @return int
     */
    public function getAttendingCount(): int
    {
        return $this->attending_count;
    }

    /**
     * @param int $attending_count
     *
     * @return Rsvp
     */
    public function setAttendingCount(int $attending_count): Rsvp
    {
        $this->attending_count = $attending_count;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaybeCount(): int
    {
        return $this->maybe_count;
    }

    /**
     * @param int $maybe_count
     *
     * @return Rsvp
     */
    public function setMaybeCount(int $maybe_count): Rsvp
    {
        $this->maybe_count = $maybe_count;

        return $this;
    }
}
