<?php declare(strict_types=1);

namespace JBours\DanceDeets;

use DateInterval;
use DateTime;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JBours\DanceDeets\Entities\Event;
use JBours\DanceDeets\Exception\DanceDeetsException;
use JBours\DanceDeets\Transformers\EventTransformer;
use stdClass;
use function array_filter;
use function array_merge;
use function json_decode;

final class DanceDeetsApiWrapper
{
    /**
     * @var Client
     */
    private $client;

    /**
     * DanceDeetsApiWrapper constructor.
     */
    public function __construct()
    {
        $this->client = new Client(
            [
                'base_uri' => 'http://www.dancedeets.com/api/v1.3/',
            ]
        );
    }


    /**
     * @param string $location
     * @param array $query
     *
     * @return Event[]
     * @throws DanceDeetsException
     * @throws Exception
     */
    public function getEventsForLocation(string $location, array $query = []): array
    {
        $locationEvents = $this->getResults($location, $query);

        $events = [];

        foreach ($locationEvents as $locationEvent) {
            $event = new EventTransformer($locationEvent);

            $events[] = $event->getObject();
        }

        return $events;
    }

    /**
     * @param string $location
     * @param array $query
     *
     * @return stdClass[]
     * @throws DanceDeetsException
     */
    public function getResults(string $location, array $query = []): array
    {
        $start = new DateTime('today');
        $end = new DateTime();
        $end->add(new DateInterval('P1Y'));

        $defaultQuery = [
            'location' => $location,
            'start'    => $start->format('Y-m-d'),
            'end'      => $end->format('Y-m-d'),
        ];

        $mergedQuery = array_merge($defaultQuery, $query);

        try {
            $response = $this->client->get('search', [
                'query' => array_filter($mergedQuery),
            ]);

            $jsonResponse = (string)$response->getBody();
            $arrayResponse = json_decode($jsonResponse, false);

            if (isset($arrayResponse->errors)) {
                throw new DanceDeetsException($arrayResponse->errors[0]);
            }

            return $arrayResponse->results;
        } catch (GuzzleException $e) {
            throw new DanceDeetsException($e->getMessage());
        }
    }
}
