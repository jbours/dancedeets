<?php

namespace JBours\DanceDeets\Transformers;

use Exception;
use JBours\DanceDeets\Collections\AdminCollection;
use JBours\DanceDeets\Collections\EventTimesCollection;
use JBours\DanceDeets\Entities\Address;
use JBours\DanceDeets\Entities\Admin;
use JBours\DanceDeets\Entities\Event;
use JBours\DanceDeets\Entities\EventTimes;
use JBours\DanceDeets\Entities\GeometryPoint;
use JBours\DanceDeets\Entities\Picture;
use JBours\DanceDeets\Entities\Source;
use JBours\DanceDeets\Entities\Venue;
use stdClass;

class EventTransformer
{
    /**
     * @var Event
     */
    protected $event;

    /**
     * EventTransformer constructor.
     *
     * @param stdClass $data
     *
     * @throws Exception
     */
    public function __construct($data)
    {
        $event = new Event();
        $event->setId($data->id);
        $event->setName($data->name);
        $event->setDescription($data->description);
        $event->setLanguage($data->language);
        $event->setStartTime($data->start_time);
        $event->setEndTime($data->end_time);
        $event->setTicketUri($data->ticket_uri);
        $event->setCategories($data->annotations->categories);
        $event->setTags($data->annotations->dance_keywords);

        $picture = $this->handlePicture($data->picture);
        $event->setPicture($picture);

        $source = $this->handleSource($data->source);
        $event->setSource($source);

        $venue = $this->handleVenue($data->venue);
        $event->setVenue($venue);

        $admins = $this->handleAdmins($data->admins);
        $event->setAdmins($admins);

        $eventTimes = $this->handleEventTimes($data->event_times);
        $event->setEventTimes($eventTimes);

        $this->event = $event;
    }

    /**
     * @return Event
     */
    public function getObject(): Event
    {
        return $this->event;
    }

    /**
     * @param stdClass $data
     *
     * @return Venue
     */
    protected function handleVenue($data): Venue
    {
        $venue = new Venue();
        $venue->setId($data->id);
        $venue->setName($data->name);

        $address = $this->handleAddress($data->address);
        $venue->setAddress($address);

        $geocode = $this->handleGeometryPoint($data->geocode);
        $venue->setGeocode($geocode);

        return $venue;
    }

    /**
     * @param stdClass $data
     *
     * @return GeometryPoint
     */
    protected function handleGeometryPoint($data): GeometryPoint
    {
        $geo = new GeometryPoint();
        $geo->setLatitude($data->latitude);
        $geo->setLongitude($data->longitude);

        return $geo;
    }

    /**
     * @param stdClass $data
     *
     * @return Address
     */
    protected function handleAddress($data): Address
    {
        $address = new Address();

        if (isset($data->street)) {
            $address->setStreet($data->street);
        }

        if (isset($data->city)) {
            $address->setCity($data->city);
        }

        if (isset($data->zip)) {
            $address->setZip($data->zip);
        }

        if (isset($data->country)) {
            $address->setCountry($data->country);
        }

        if (isset($data->countryCode)) {
            $address->setCountryCode($data->countryCode);
        }

        return $address;
    }

    /**
     * @param stdClass $data
     *
     * @return Source
     */
    protected function handleSource($data): Source
    {
        $source = new Source();
        $source->setName($data->name);
        $source->setUrl($data->url);

        return $source;
    }

    /**
     * @param array $data
     *
     * @return AdminCollection|Admin[]
     * @throws Exception
     */
    protected function handleAdmins($data): AdminCollection
    {
        $admins = new AdminCollection();

        foreach ($data as $item) {
            $admin = new Admin();
            $admin->setId($item->id);
            $admin->setName($item->name);
            $admins->getIterator()->append($admin);
        }

        return $admins;
    }

    /**
     * @param stdClass $data
     *
     * @return Picture
     */
    protected function handlePicture($data): Picture
    {
        $picture = new Picture();

        if ($data) {
            $picture->setSource($data->source);
            $picture->setWidth($data->width);
            $picture->setHeight($data->height);
        }

        return $picture;
    }

    /**
     * @param array $data
     *
     * @return EventTimesCollection|EventTimes[]
     * @throws Exception
     */
    protected function handleEventTimes($data): EventTimesCollection
    {
        $eventTimes = new EventTimesCollection();

        if (!$data) {
            return $eventTimes;
        }

        foreach ($data as $obj) {
            $eventTime = new EventTimes();
            $eventTime->setStartTime($obj->start_time);
            if (isset($obj->end_time)) {
                $eventTime->setEndTime($obj->end_time);
            }

            $eventTimes->getIterator()->append($eventTime);
        }

        return $eventTimes;
    }
}
