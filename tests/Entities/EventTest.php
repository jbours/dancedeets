<?php

namespace JBours\Tests\DanceDeets\Entities;

use ArgumentCountError;
use JBours\DanceDeets\Collections\AdminCollection;
use JBours\DanceDeets\Collections\EventTimesCollection;
use JBours\DanceDeets\Entities\Admin;
use JBours\DanceDeets\Entities\Event;
use JBours\DanceDeets\Entities\EventTimes;
use JBours\DanceDeets\Entities\Picture;
use JBours\DanceDeets\Entities\Source;
use JBours\DanceDeets\Entities\Venue;
use JBours\Tests\DanceDeets\TestCase;
use TypeError;

class EventTest extends TestCase
{
    /**
     * @var \JBours\DanceDeets\Entities\Event
     */
    protected $event;

    public function setUp()
    {
        $this->event = new Event();
    }

    /**
     * @test
     */
    public function itCanSetADescriptionValue()
    {
        $this->event->setDescription('Lorem ipsum dolar imit.');

        $this->assertEquals('Lorem ipsum dolar imit.', $this->event->getDescription());
    }

    /**
     * @test
     */
    public function itCannotSetADescriptionNullValue()
    {
        $this->expectException(TypeError::class);

        $this->event->setDescription(null);
    }

    /**
     * @test
     */
    public function itCanSetALanguageValue()
    {
        $this->event->setLanguage('nl');

        $this->assertEquals('nl', $this->event->getLanguage());
    }

    /**
     * @test
     */
    public function itCanSetALanguageNullValue()
    {
        $this->event->setLanguage();

        $this->assertNull($this->event->getLanguage());
    }

    /**
     * @test
     */
    public function itCanSetACategoriesValue()
    {
        $categories = ['Breaking', 'Competition', 'All Style'];

        $this->event->setCategories($categories);

        $this->assertEquals($categories, $this->event->getCategories());
    }

    /**
     * @test
     */
    public function itCannotSetACategoriesStringValue()
    {
        $this->expectException(TypeError::class);

        $this->event->setCategories('lorem ipsum');
    }

    /**
     * @test
     */
    public function itCannotSetACategoriesNullValue()
    {
        $this->expectException(ArgumentCountError::class);

        $this->event->setCategories();
    }

    /**
     * @test
     */
    public function itCanSetATagsValue()
    {
        $tags = ['1on1', 'freestyle', 'cypher'];

        $this->event->setTags($tags);

        $this->assertEquals($tags, $this->event->getTags());
    }

    /**
     * @test
     */
    public function itCannotSetATagsStringValue()
    {
        $this->expectException(TypeError::class);

        $this->event->setTags('lorem ipsum');
    }

    /**
     * @test
     */
    public function itCannotSetATagsNullValue()
    {
        $this->expectException(ArgumentCountError::class);

        $this->event->setTags();
    }

    /**
     * @test
     */
    public function itCanSetATicketUriValue()
    {
        $this->event->setTicketUri('http://www.example.com/tickets');

        $this->assertEquals('http://www.example.com/tickets', $this->event->getTicketUri());
    }

    /**
     * @test
     */
    public function itCanSetATicketUriNullValue()
    {
        $this->event->setTicketUri();

        $this->assertNull($this->event->getTicketUri());
    }

    /**
     * @test
     */
    public function itCanSetAPictureObjectAsValue()
    {
        $picture = new Picture();
        $picture->setSource('https://lorempixel.com/640/480');
        $picture->setWidth(640);
        $picture->setHeight(400);

        $this->event->setPicture($picture);

        $this->assertEquals($picture, $this->event->getPicture());
    }

    /**
     * @test
     */
    public function itCanSetAnEmptyPictureObjectAsValue()
    {
        $this->event->setPicture(new Picture());

        $this->assertInstanceOf(Picture::class, $this->event->getPicture());
    }

    /**
     * @test
     */
    public function itCannotSetAPictureNullValue()
    {
        $this->expectException(TypeError::class);

        $this->event->setPicture(null);
    }

    /**
     * @test
     */
    public function itCanSetASourceObjectAsValueForSource()
    {
        $source = new Source();
        $source->setUrl('http://facebook.com/1234567890');
        $source->setName('Facebook Event');

        $this->event->setSource($source);

        $this->assertEquals($source, $this->event->getSource());
        $this->assertInstanceOf(Source::class, $this->event->getSource());
    }

    /**
     * @test
     */
    public function itCanSetAnEmptySourceObjectAsValueForSource()
    {
        $source = new Source();

        $this->event->setSource($source);

        $this->assertEquals($source, $this->event->getSource());
        $this->assertInstanceOf(Source::class, $this->event->getSource());
    }

    /**
     * @test
     */
    public function itCannotSetNullAsValueForSource()
    {
        $this->expectException(TypeError::class);

        $this->event->setSource(null);
    }

    /**
     * @test
     */
    public function itCannotSetNoValueForSource()
    {
        $this->expectException(ArgumentCountError::class);

        $this->event->setSource();
    }

    /**
     * @test
     */
    public function itCanSetAnAdminCollectionAsValueForAdmins()
    {
        $johnDoe = new Admin();
        $johnDoe->setName('John Doe');
        $johnDoe->setId(1234567890);

        $janeDoe = new Admin();
        $janeDoe->setName('Jane Doe');
        $janeDoe->setId(9876543210);

        $admins = new AdminCollection($johnDoe, $janeDoe);

        $this->event->setAdmins($admins);

        $this->assertEquals($admins, $this->event->getAdmins());
    }

    /**
     * @test
     */
    public function itCanSetAnEmptyAdminCollectionAsValueForAdmins()
    {
        $admins = new AdminCollection();

        $this->event->setAdmins($admins);

        $this->assertEquals($admins, $this->event->getAdmins());
    }

    /**
     * @test
     */
    public function itCannotSetAnEmptyArrayAsValueForAdmins()
    {
        $this->expectException(TypeError::class);

        $this->event->setAdmins([]);
    }

    /**
     * @test
     */
    public function itCannotSetNullAsValueForAdmins()
    {
        $this->expectException(TypeError::class);

        $this->event->setAdmins(null);
    }

    /**
     * @test
     */
    public function itCannotSetNoValueForAdmins()
    {
        $this->expectException(ArgumentCountError::class);

        $this->event->setAdmins();
    }

    /**
     * @test
     */
    public function itCanSetAnEventTimesCollectionAsValueForEventTimes()
    {
        $one = new EventTimes();
        $one->setStartTime('2018-07-30T11:00:00+0200');
        $one->setEndTime('2018-07-30T17:00:00+0200');

        $two = new EventTimes();
        $two->setStartTime('2018-07-31T11:00:00+0200');
        $two->setEndTime('2018-07-31T17:00:00+0200');

        $eventTimes = new EventTimesCollection($one, $two);

        $this->event->setEventTimes($eventTimes);

        $this->assertEquals($eventTimes, $this->event->getEventTimes());
    }

    /**
     * @test
     */
    public function itCanSetAnEmptyEventTimesCollectionAsValueForEventTimes()
    {
        $eventTimes = new EventTimesCollection();

        $this->event->setEventTimes($eventTimes);

        $this->assertEquals($eventTimes, $this->event->getEventTimes());
    }

    /**
     * @test
     */
    public function itCannotSetAnEmptyArrayAsValueForEventTimes()
    {
        $this->expectException(TypeError::class);

        $this->event->setEventTimes([]);
    }

    /**
     * @test
     */
    public function itCannotSetNullAsValueForEventTimes()
    {
        $this->expectException(TypeError::class);

        $this->event->setEventTimes(null);
    }

    /**
     * @test
     */
    public function itCannotSetNoValueForEventTimes()
    {
        $this->expectException(ArgumentCountError::class);

        $this->event->setEventTimes();
    }

    /**
     * @test
     */
    public function itCanSetAVenueObjectAsValueForVenue()
    {
        $venue = new Venue();
        $venue->setId(123456789);
        $venue->setName('Some venue');

        $this->event->setVenue($venue);

        $this->assertEquals($venue, $this->event->getVenue());
        $this->assertInstanceOf(Venue::class, $this->event->getVenue());
    }

    /**
     * @test
     */
    public function itCanSetAnEmptyVenueObjectAsValueForVenue()
    {
        $venue = new Venue();

        $this->event->setVenue($venue);

        $this->assertEquals($venue, $this->event->getVenue());
        $this->assertInstanceOf(Venue::class, $this->event->getVenue());
    }

    /**
     * @test
     */
    public function itCannotSetNullAsValueForVenue()
    {
        $this->expectException(TypeError::class);

        $this->event->setVenue(null);
    }

    /**
     * @test
     */
    public function itCannotSetNosValueForVenue()
    {
        $this->expectException(ArgumentCountError::class);

        $this->event->setVenue();
    }
}
