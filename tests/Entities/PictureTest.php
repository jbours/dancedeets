<?php

declare(strict_types=1);

namespace JBours\Tests\DanceDeets\Entities;

use ArgumentCountError;
use JBours\DanceDeets\Entities\Picture;
use JBours\Tests\DanceDeets\TestCase;
use TypeError;

class PictureTest extends TestCase
{
    protected $picture;

    public function setUp()
    {
        parent::setUp();

        $this->picture = new Picture();
    }

    /**
     * @test
     */
    public function itCanSetAStringValueForSource()
    {
        $this->picture->setSource('https://lorempixel.com/640/480');

        $this->assertEquals('https://lorempixel.com/640/480', $this->picture->getSource());
    }

    /**
     * @test
     */
    public function itCannotSetANullValueForSource()
    {
        $this->expectException(TypeError::class);

        $this->picture->setSource(null);
    }

    /**
     * @test
     */
    public function itCannotSetANoValueForSource()
    {
        $this->expectException(ArgumentCountError::class);

        $this->picture->setSource();
    }

    /**
     * @test
     */
    public function itCanSetAIntegerValueFoWidth()
    {
        $this->picture->setWidth(640);

        $this->assertEquals(640, $this->picture->getWidth());
    }

    /**
     * @test
     */
    public function itCannotSetStringValueForWidth()
    {
        $this->expectException(TypeError::class);

        $this->picture->setWidth('640');
    }

    /**
     * @test
     */
    public function itCannotSetANullValueForWidth()
    {
        $this->expectException(TypeError::class);

        $this->picture->setWidth(null);
    }

    /**
     * @test
     */
    public function itCannotSetANoValueForWidth()
    {
        $this->expectException(ArgumentCountError::class);

        $this->picture->setWidth();
    }

    /**
     * @test
     */
    public function itCanSetAIntegerValueFoHeight()
    {
        $this->picture->setHeight(480);

        $this->assertEquals(480, $this->picture->getHeight());
    }

    /**
     * @test
     */
    public function itCannotSetStringValueForHeight()
    {
        $this->expectException(TypeError::class);

        $this->picture->setHeight('480');
    }

    /**
     * @test
     */
    public function itCannotSetANullValueForHeight()
    {
        $this->expectException(TypeError::class);

        $this->picture->setHeight(null);
    }

    /**
     * @test
     */
    public function itCannotSetANoValueForHeight()
    {
        $this->expectException(ArgumentCountError::class);

        $this->picture->setHeight();
    }
}
