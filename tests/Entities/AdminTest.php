<?php

namespace JBours\Tests\DanceDeets\Entities;

use JBours\DanceDeets\Entities\Admin;
use JBours\Tests\DanceDeets\TestCase;

class AdminTest extends TestCase
{
    protected $admin;

    public function setUp()
    {
        $this->admin = new Admin();
    }

    /**
     * @test
     */
    public function itCanSetValueToId()
    {
        $this->admin->setId('1232143712973');

        $this->assertEquals('1232143712973', $this->admin->getId());
    }

    /**
     * @test
     */
    public function itCannotSetANullValueToId()
    {
        $this->expectException(\ArgumentCountError::class);

        $this->admin->setId();
    }

    /**
     * @test
     */
    public function itCanSetValueToName()
    {
        $this->admin->setName('John Doe');

        $this->assertEquals('John Doe', $this->admin->getName());
    }

    /**
     * @test
     */
    public function itCannotSetANullValueToName()
    {
        $this->expectException(\ArgumentCountError::class);

        $this->admin->setName();
    }
}
