<?php

namespace JBours\DanceDeets\Traits;

/**
 * Trait NameAwareEntityTrait
 *
 * @package JBours\DanceDeets\Traits
 */
trait NameAwareEntityTrait
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
