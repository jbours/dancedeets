<?php

namespace JBours\DanceDeets\Collections;

use ArrayIterator;
use IteratorAggregate;
use JBours\DanceDeets\Entities\Admin;

class AdminCollection implements IteratorAggregate
{
    protected $admins;

    public function __construct(Admin ...$admins)
    {
        $this->admins = $admins;
    }

    public function getIterator()
    {
        return new ArrayIterator($this->admins);
    }
}
