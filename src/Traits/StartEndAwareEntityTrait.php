<?php

namespace JBours\DanceDeets\Traits;

use DateTime;

trait StartEndAwareEntityTrait
{
    /**
     * @var \DateTime;
     */
    protected $start_time;

    /**
     * @var \DateTime;
     */
    protected $end_time;

    /**
     * @return \DateTime
     */
    public function getStartTime(): DateTime
    {
        return $this->start_time;
    }

    /**
     * @param string|\DateTime $start_time
     *
     * @return self
     */
    public function setStartTime($start_time): self
    {
        if (!$start_time instanceof DateTime) {
            $this->start_time = new DateTime($start_time);
        } else {
            $this->start_time = $start_time;
        }

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getEndTime()
    {
        return $this->end_time;
    }

    /**
     * @param string|\DateTime $end_time
     *
     * @return self
     */
    public function setEndTime($end_time): self
    {
        if (!$end_time instanceof DateTime) {
            $this->end_time = $end_time ? new DateTime($end_time) : null;
        } else {
            $this->end_time = $end_time;
        }

        return $this;
    }
}
