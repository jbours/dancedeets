<?php

namespace JBours\Tests\DanceDeets\Entities;

use JBours\DanceDeets\Entities\Address;
use JBours\Tests\DanceDeets\TestCase;

class AddressTest extends TestCase
{
    /**
     * @var \JBours\DanceDeets\Entities\Address
     */
    protected $address;

    public function setUp()
    {
        $this->address = new Address();
    }

    /**
     * @test
     */
    public function itSetsAnStreet()
    {
        $this->address->setStreet('Some street 1');

        $this->assertEquals('Some street 1', $this->address->getStreet());
    }

    /**
     * @test
     */
    public function itSetNullStreet()
    {
        $this->address->setStreet();

        $this->assertEquals(null, $this->address->getStreet());
    }

    /**
     * @test
     */
    public function itSetsCityValue()
    {
        $this->address->setCity('Some city');

        $this->assertEquals('Some city', $this->address->getCity());
    }

    /**
     * @test
     */
    public function itSetsCityNullValue()
    {
        $this->address->setCity();

        $this->assertEquals(null, $this->address->getCity());
    }

    /**
     * @test
     */
    public function itSetsCountryCodeValue()
    {
        $this->address->setCountryCode('nl');

        $this->assertEquals('nl', $this->address->getCountryCode());
    }

    /**
     * @test
     */
    public function itSetsCountryCodeNullValue()
    {
        $this->address->setCountryCode();

        $this->assertEquals(null, $this->address->getCountryCode());
    }

    /**
     * @test
     */
    public function itSetsCountryValue()
    {
        $this->address->setCountry('Netherlands');

        $this->assertEquals('Netherlands', $this->address->getCountry());
    }

    /**
     * @test
     */
    public function itSetsCountryNullValue()
    {
        $this->address->setCountryCode();

        $this->assertEquals(null, $this->address->getCountry());
    }

    /**
     * @test
     */
    public function itSetsZipValue()
    {
        $this->address->setZip('1000AA');

        $this->assertEquals('1000AA', $this->address->getZip());
    }

    /**
     * @test
     */
    public function itSetsZipNullValue()
    {
        $this->address->setCountryCode();

        $this->assertEquals(null, $this->address->getZip());
    }
}
