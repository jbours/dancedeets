<?php

namespace JBours\DanceDeets\Entities;

use JBours\DanceDeets\Traits\NameAwareEntityTrait;

class Source
{
    use NameAwareEntityTrait;

    /**
     * @var string
     */
    protected $url;

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return Source
     */
    public function setUrl(string $url): Source
    {
        $this->url = $url;

        return $this;
    }
}
