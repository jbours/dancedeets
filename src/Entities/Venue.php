<?php

namespace JBours\DanceDeets\Entities;

use JBours\DanceDeets\Traits\IdAwareEntityTrait;
use JBours\DanceDeets\Traits\NameAwareEntityTrait;

class Venue
{
    use IdAwareEntityTrait;
    use NameAwareEntityTrait;

    /**
     * @var \JBours\DanceDeets\Entities\Address
     */
    protected $address;
    /**
     * @var \JBours\DanceDeets\Entities\GeometryPoint
     */
    protected $geocode;

    /**
     * @return string|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     *
     * @return \JBours\DanceDeets\Entities\Venue
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return \JBours\DanceDeets\Entities\Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @param \JBours\DanceDeets\Entities\Address $address
     *
     * @return Venue
     */
    public function setAddress(Address $address): Venue
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return \JBours\DanceDeets\Entities\GeometryPoint
     */
    public function getGeocode(): GeometryPoint
    {
        return $this->geocode;
    }

    /**
     * @param \JBours\DanceDeets\Entities\GeometryPoint $geocode
     *
     * @return Venue
     */
    public function setGeocode(GeometryPoint $geocode): Venue
    {
        $this->geocode = $geocode;

        return $this;
    }
}
