<?php

namespace JBours\Tests\DanceDeets\Traits;

use JBours\DanceDeets\Traits\NameAwareEntityTrait;
use JBours\Tests\DanceDeets\TestCase;

class NameAwareEntityTraitTest extends TestCase
{
    protected $stub;

    public function setUp()
    {
        parent::setUp();

        $this->stub = $this->getMockForTrait(NameAwareEntityTrait::class);
    }

    /**
     * @test
     */
    public function itCanSetANameValue()
    {
        $this->stub->setName('John Doe');

        $this->assertEquals('John Doe', $this->stub->getName());
    }

    /**
     * @test
     */
    public function itCannotSetNullNameValue()
    {
        $this->expectException(\ArgumentCountError::class);

        $this->stub->setName();
    }
}
