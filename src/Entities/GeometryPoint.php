<?php

declare(strict_types=1);

namespace JBours\DanceDeets\Entities;

class GeometryPoint
{
    /**
     * @var float
     */
    protected $latitude;
    /**
     * @var float
     */
    protected $longitude;

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     *
     * @return GeometryPoint
     */
    public function setLatitude(float $latitude): GeometryPoint
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     *
     * @return GeometryPoint
     */
    public function setLongitude(float $longitude): GeometryPoint
    {
        $this->longitude = $longitude;

        return $this;
    }
}
