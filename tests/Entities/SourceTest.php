<?php

namespace JBours\Tests\DanceDeets\Entities;

use JBours\DanceDeets\Entities\Source;
use JBours\Tests\DanceDeets\TestCase;

class SourceTest extends TestCase
{
    protected $source;

    public function setUp()
    {
        $this->source = new Source();
    }

    /**
     * @test
     */
    public function itCanSetAnUrlStringValue()
    {
        $this->source->setUrl('http://www.example.com');

        $this->assertEquals('http://www.example.com', $this->source->getUrl());
    }

    /**
     * @test
     */
    public function itCannotSetAnUrlNullValue()
    {
        $this->expectException(\TypeError::class);

        $this->source->setUrl(null);
    }
}
