<?php

declare(strict_types=1);

namespace JBours\DanceDeets\Entities;

class Picture
{
    /**
     * @var string
     */
    protected $source;
    /**
     * @var int
     */
    protected $width;

    /**
     * @var int
     */
    protected $height;

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     *
     * @return Picture
     */
    public function setSource(string $source): Picture
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     *
     * @return Picture
     */
    public function setWidth(int $width): Picture
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     *
     * @return Picture
     */
    public function setHeight(int $height): Picture
    {
        $this->height = $height;

        return $this;
    }
}
