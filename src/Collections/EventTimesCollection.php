<?php

namespace JBours\DanceDeets\Collections;

use ArrayIterator;
use IteratorAggregate;
use JBours\DanceDeets\Entities\EventTimes;

class EventTimesCollection implements IteratorAggregate
{
    protected $eventTimes;

    public function __construct(EventTimes ...$eventTimes)
    {
        $this->eventTimes = $eventTimes;
    }

    public function getIterator()
    {
        return new ArrayIterator($this->eventTimes);
    }
}
