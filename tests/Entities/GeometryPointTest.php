<?php

declare(strict_types=1);

namespace JBours\Tests\DanceDeets\Entities;

use ArgumentCountError;
use JBours\DanceDeets\Entities\GeometryPoint;
use JBours\Tests\DanceDeets\TestCase;
use TypeError;

class GeometryPointTest extends TestCase
{
    protected $geometryPoint;

    public function setUp()
    {
        parent::setUp();

        $this->geometryPoint = new GeometryPoint();
    }

    /**
     * @test
     */
    public function itCanSetAFloatValueForLatitude()
    {
        $this->geometryPoint->setLatitude(53.2181718);

        $this->assertEquals(53.2181718, $this->geometryPoint->getLatitude());
    }

    /**
     * @test
     */
    public function itCannotSetAStringValueForLatitude()
    {
        $this->expectException(TypeError::class);

        $this->geometryPoint->setLatitude('53.2181718');
    }

    /**
     * @test
     */
    public function itCannotSetAnNullValueForLatitude()
    {
        $this->expectException(TypeError::class);

        $this->geometryPoint->setLatitude(null);
    }

    /**
     * @test
     */
    public function itCannotSetNoValueForLatitude()
    {
        $this->expectException(ArgumentCountError::class);

        $this->geometryPoint->setLatitude();
    }

    /**
     * @test
     */
    public function itCanSetAFloatValueForLongitude()
    {
        $this->geometryPoint->setLongitude(6.5686807);

        $this->assertEquals(6.5686807, $this->geometryPoint->getLongitude());
    }

    /**
     * @test
     */
    public function itCannotSetAStringValueForLongitude()
    {
        $this->expectException(TypeError::class);

        $this->geometryPoint->setLongitude('6.5686807');
    }

    /**
     * @test
     */
    public function itCannotSetAnNullValueForLongitude()
    {
        $this->expectException(TypeError::class);

        $this->geometryPoint->setLongitude(null);
    }

    /**
     * @test
     */
    public function itCannotSetNoValueForLongitude()
    {
        $this->expectException(ArgumentCountError::class);

        $this->geometryPoint->setLongitude();
    }
}
