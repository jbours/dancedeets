<?php

namespace JBours\Tests\DanceDeets\Traits;

use JBours\DanceDeets\Traits\StartEndAwareEntityTrait;
use JBours\Tests\DanceDeets\TestCase;

class StartEndAwareEntityTraitTest extends TestCase
{
    use StartEndAwareEntityTrait;

    /**
     * @test
     */
    public function itSetsAnJsonStartDate()
    {
        $this->setStartTime('2017-12-17T13:00:00+0100');

        $this->assertInstanceOf(\DateTime::class, $this->getStartTime());
    }

    /**
     * @test
     */
    public function itSetsAnDateTimeStartDate()
    {
        $this->setStartTime(new \DateTime());

        $this->assertInstanceOf(\DateTime::class, $this->getStartTime());
    }

    /**
     * @test
     */
    public function itSetsAnJsonEndDate()
    {
        $this->setEndTime('2017-12-17T18:00:00+0100');

        $this->assertInstanceOf(\DateTime::class, $this->getEndTime());
    }

    /**
     * @test
     */
    public function itSetsAnDateTimeEndDate()
    {
        $this->setEndTime(new \DateTime());

        $this->assertInstanceOf(\DateTime::class, $this->getEndTime());
    }
}
