<?php

namespace JBours\Tests\DanceDeets;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use JBours\DanceDeets\DanceDeetsApiWrapper;
use JBours\DanceDeets\Entities\Event;
use JBours\DanceDeets\Entities\Picture;
use JBours\DanceDeets\Entities\Source;
use JBours\DanceDeets\Entities\Venue;
use JBours\DanceDeets\Exception\DanceDeetsException;

/**
 * Class DanceDeetsApiWrapperTest
 *
 * @package JBours\Tests\DanceDeets
 */
class DanceDeetsApiWrapperTest extends TestCase
{
    /**
     * @var array
     */
    protected $events;

    /**
     * @var DanceDeetsApiWrapper
     */
    protected $api;

    /**
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->api = new DanceDeetsApiWrapper();
    }

    /**
     * @test
     */
    public function itCatchesGuzzleException()
    {
        $mock = new MockHandler(
            [
            new RequestException('Error Communicating with Server', new Request('GET', 'test'), new Response(500))
            ]
        );

        $handler =HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $this->expectException(GuzzleException::class);
        $this->expectExceptionMessage('Error Communicating with Server');
        $this->expectExceptionCode(500);

        $client->request('GET', 'test');
    }

    /**
     * @test
     */
    public function itThrowAnExceptionOnInvalidCountry()
    {
        $this->expectException(DanceDeetsException::class);

        $this->api->getResults('blabla');
    }

    /**
     * @test
     */
    public function itCannotReceiveEvents()
    {
        $this->expectException(DanceDeetsException::class);

        $events = $this->api->getEventsForLocation('netherlands');

        $this->assertInternalType('array', $events);
    }

//    /**
//     * @test
//     */
//    public function itCanReceiveEvents()
//    {
//        $events = $this->api->getEventsForLocation('netherlands');
//
//        $this->assertInternalType('array', $events);
//    }
//
//    /**
//     * @test
//     */
//    public function eventHasAttributes()
//    {
//        $events = $this->api->getEventsForLocation('netherlands');
//        /**
//         * @var Event $object
//         */
//        $object = $events[0];
//
//        $this->assertObjectHasAttribute('id', $object);
//        $this->assertObjectHasAttribute('picture', $object);
//        $this->assertInstanceOf(Picture::class, $object->getPicture());
//        $this->assertObjectHasAttribute('name', $object);
//        $this->assertObjectHasAttribute('description', $object);
//        $this->assertObjectHasAttribute('source', $object);
//        $this->assertInstanceOf(Source::class, $object->getSource());
//        $this->assertObjectHasAttribute('venue', $object);
//        $this->assertInstanceOf(Venue::class, $object->getVenue());
//        $this->assertObjectHasAttribute('start_time', $object);
//        $this->assertObjectHasAttribute('end_time', $object);
//        $this->assertObjectHasAttribute('ticket_uri', $object);
//        $this->assertObjectHasAttribute('admins', $object);
//    }
}
