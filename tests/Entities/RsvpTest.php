<?php

declare(strict_types=1);

namespace JBours\Tests\DanceDeets\Entities;

use JBours\DanceDeets\Entities\Rsvp;
use JBours\Tests\DanceDeets\TestCase;

class RsvpTest extends TestCase
{
    protected $rsvp;

    public function setUp()
    {
        parent::setUp();

        $this->rsvp = new Rsvp();
    }

    /**
     * @test
     */
    public function itCanSetAnIntegerValueForAttending()
    {
        $this->rsvp->setAttendingCount(98);

        $this->assertEquals(98, $this->rsvp->getAttendingCount());
    }

    /**
     * @test
     */
    public function itCannotSetAStringValueForAttending()
    {
        $this->expectException(\TypeError::class);

        $this->rsvp->setAttendingCount('98');
    }

    /**
     * @test
     */
    public function itCannotSetANullValueForAttending()
    {
        $this->expectException(\TypeError::class);

        $this->rsvp->setAttendingCount(null);
    }

    /**
     * @test
     */
    public function itCanSetAnIntegerValueForMaybe()
    {
        $this->rsvp->setMaybeCount(356);

        $this->assertEquals(356, $this->rsvp->getMaybeCount());
    }

    /**
     * @test
     */
    public function itCannotSetAStringValueForMaybe()
    {
        $this->expectException(\TypeError::class);

        $this->rsvp->setMaybeCount('98');
    }

    /**
     * @test
     */
    public function itCannotSetANullValueForMaybe()
    {
        $this->expectException(\TypeError::class);

        $this->rsvp->setMaybeCount(null);
    }
}
