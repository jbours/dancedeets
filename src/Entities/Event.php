<?php

namespace JBours\DanceDeets\Entities;

use JBours\DanceDeets\Collections\AdminCollection;
use JBours\DanceDeets\Collections\EventTimesCollection;
use JBours\DanceDeets\Traits\IdAwareEntityTrait;
use JBours\DanceDeets\Traits\NameAwareEntityTrait;
use JBours\DanceDeets\Traits\StartEndAwareEntityTrait;

class Event
{
    use IdAwareEntityTrait;
    use NameAwareEntityTrait;
    use StartEndAwareEntityTrait;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var \JBours\DanceDeets\Entities\Picture
     */
    protected $picture;

    /**
     * @var string
     */
    protected $language;

    /**
     * @var \JBours\DanceDeets\Entities\Source
     */
    protected $source;

    /**
     * @var string[]
     */
    protected $categories;

    /**
     * @var string[]
     */
    protected $tags;

    /**
     * @var string
     */
    protected $ticket_uri;

    /**
     * @var \JBours\DanceDeets\Collections\AdminCollection
     */
    protected $admins;

    /**
     * @var \JBours\DanceDeets\Collections\EventTimesCollection
     */
    protected $event_times;

    /**
     * @var \JBours\DanceDeets\Entities\Venue
     */
    protected $venue;

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Event
     */
    public function setDescription(string $description): Event
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return \JBours\DanceDeets\Entities\Picture
     */
    public function getPicture(): Picture
    {
        return $this->picture;
    }

    /**
     * @param \JBours\DanceDeets\Entities\Picture $picture
     *
     * @return Event
     */
    public function setPicture(Picture $picture): Event
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string|null $language
     *
     * @return Event
     */
    public function setLanguage($language = null): Event
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return \JBours\DanceDeets\Entities\Source
     */
    public function getSource(): Source
    {
        return $this->source;
    }

    /**
     * @param \JBours\DanceDeets\Entities\Source $source
     *
     * @return Event
     */
    public function setSource(Source $source): Event
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @param string[] $categories
     *
     * @return Event
     */
    public function setCategories(array $categories): Event
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param string[] $tags
     *
     * @return Event
     */
    public function setTags(array $tags): Event
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTicketUri()
    {
        return $this->ticket_uri;
    }

    /**
     * @param string|null $ticket_uri
     *
     * @return Event
     */
    public function setTicketUri($ticket_uri = null): Event
    {
        $this->ticket_uri = $ticket_uri;

        return $this;
    }

    /**
     * @return \JBours\DanceDeets\Collections\AdminCollection
     */
    public function getAdmins(): AdminCollection
    {
        return $this->admins;
    }

    /**
     * @param \JBours\DanceDeets\Collections\AdminCollection $admins
     *
     * @return Event
     */
    public function setAdmins(AdminCollection $admins): Event
    {
        $this->admins = $admins;

        return $this;
    }

    /**
     * @return \JBours\DanceDeets\Collections\EventTimesCollection
     */
    public function getEventTimes(): EventTimesCollection
    {
        return $this->event_times;
    }

    /**
     * @param \JBours\DanceDeets\Collections\EventTimesCollection $event_times
     *
     * @return Event
     */
    public function setEventTimes(EventTimesCollection $event_times): Event
    {
        $this->event_times = $event_times;

        return $this;
    }

    /**
     * @return \JBours\DanceDeets\Entities\Venue
     */
    public function getVenue(): Venue
    {
        return $this->venue;
    }

    /**
     * @param \JBours\DanceDeets\Entities\Venue $venue
     *
     * @return Event
     */
    public function setVenue(Venue $venue): Event
    {
        $this->venue = $venue;

        return $this;
    }
}
